import * as path from 'path'

const TEST_PORT = 8901
const TEST_ADDRESS = '127.0.0.1'
const FILE_PATH_UPLOADS: string = path.resolve(__dirname, './uploads')
const FILE_PATH_UNZIPPED: string = path.resolve(__dirname, './unzipped')

export {
  TEST_PORT,
  TEST_ADDRESS,
  FILE_PATH_UPLOADS,
  FILE_PATH_UNZIPPED
}
