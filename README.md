# 🍎 [app.enoplayapis.com](https://app.enoplayapis.com)

> API for managing apps for Enoplay.

## 📘 [Documentation](https://gitlab.com/enoplay-org/com-enoplayapis-app/tree/master/doc)

## 🔨 Build Setup

```bash
# install dependencies
npm install

# serve with live reload at localhost:8900
npm run dev

# run unit tests
npm run unit

# build for production
npm run build

# run production server
npm run prod
```
