interface SessionTokenRepository {
  verifyAccess (tokenString: string): Promise<SessionTokenClaim>
}

class SessionTokenClaim {
  userUID: string
  type: string
  dateExpired: Date
  dateIssued: Date
  jti: string
}

const SESSION_TOKEN_TYPE_ACCESS = 'access'

export {
  SessionTokenRepository,
  SessionTokenClaim,
  SESSION_TOKEN_TYPE_ACCESS
}
