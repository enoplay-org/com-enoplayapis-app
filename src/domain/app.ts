interface AppRepository {
  getByUID (uid: string): Promise<App>
  getByAID (aid: string): Promise<App>
  getStatus (app: App): Promise<AppStatus>

  create (app: App, language: string, filePath: string): Promise<App>
  updateVersionsActive (app: App): Promise<App>
  updateFile (app: App, language: string, filePath: string, hostAppID?: string): Promise<App>
  deleteWithForce (app: App): Promise<boolean>
  existsByAID (aid: string): Promise<boolean>

  getVersion (appUID: string, versionUID: string): Promise<AppVersionHistoryItem>
  getVersionHistory (appUID: string): Promise<AppVersionHistory>
  addVersion (appUID: string, version: AppVersionHistoryItem): Promise<boolean>
}

class App {
  uid: string
  aid: string
  title: string
  status: string
  versionsActive: AppVersionsActive
  dateCreated: Date
  publisher: {
    uid: string
    username: string
  }
  game: {
    uid: string
    gid: string
  }
  constructor () {
    this.versionsActive = new AppVersionsActive()
    this.publisher = { uid: '', username: '' }
    this.game = { uid: '', gid: '' }
  }
}

class AppVersionsActive {
  prod: string
  dev: string
}

// AppVersionHistory contains a list of versions for a given App
class AppVersionHistory {
  appUID: string
  versions: { [key: string]: AppVersionHistoryItem }
  constructor () {
    this.versions = {}
  }
}

class AppVersionHistoryItem {
  uid: string
  title: string
  language: string
  gitRepoDetails: GitRepoDetails
  hostDetails: HostDetails
  constructor () {
    this.gitRepoDetails = new GitRepoDetails()
    this.hostDetails = new HostDetails()
  }
}

class AppStatus {
  appUID: string
  versionsActive: {
    prod: {
      hostDetailsStatus: HostDetailsStatus
    }
    dev: {
      hostDetailsStatus: HostDetailsStatus
    }
  }
  constructor () {
    this.versionsActive = {
      prod: { hostDetailsStatus: new HostDetailsStatus() },
      dev: { hostDetailsStatus: new HostDetailsStatus() }
    }
  }
}

class GitRepoDetails {
  service: string
  repoID: string
  name: string
  repoURL: string
  branch: string
  tarball: string
}

class HostDetails {
  service: string
  appID: string
  webURL: string
  appSetupID: string
}

class HostDetailsStatus {
  label: string
  message: string
  buildLogDetails: string
  runtimeLogDetails: string
}

const APP_LANGUAGE_SPA: string = 'spa'
const APP_LANGUAGE_NODE: string = 'node'
const APP_LANGUAGE_GO: string = 'go'

const APP_REPO_GITLAB: string = 'com-gitlab-www'
const APP_REPO_ENOPLAY_GITLAB: string = 'com-enoplay-gitlab'

const APP_HOST_HEROKU: string = 'com-heroku-www'

const APP_STATUS_PENDING: string = 'pending'
const APP_STATUS_ACTIVE: string = 'active'
const APP_STATUS_ERROR: string = 'error'
const APP_STATUS_DELETED: string = 'deleted'

export {
  App,
  AppRepository,
  AppVersionsActive,
  AppVersionHistory,
  AppVersionHistoryItem,
  AppStatus,
  GitRepoDetails,
  HostDetails,
  HostDetailsStatus,
  APP_LANGUAGE_SPA,
  APP_LANGUAGE_NODE,
  APP_LANGUAGE_GO,
  APP_REPO_GITLAB,
  APP_REPO_ENOPLAY_GITLAB,
  APP_HOST_HEROKU,
  APP_STATUS_PENDING,
  APP_STATUS_ACTIVE,
  APP_STATUS_ERROR,
  APP_STATUS_DELETED
}
