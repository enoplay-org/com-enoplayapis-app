import * as path from 'path'
import * as fse from 'fs-extra'
import axios from 'axios'
import * as git from 'nodegit'

import { GitRepoDetails } from '../domain/app'

const branchPrefix: string = 'version_'

class GitlabOptions {
  apiBase: string
  apiDomain: string
  token: string
  serviceUID: string
  serviceName: string
  author: GitlabOptionsAuthor
  namespace: GitlabOptionsNamespace // { ID: 1234567, name: 'enoplay-games', description: 'Enoplay Games'}
  remoteName: string // enoplay
  constructor (apiBase: string, apiDomain: string, token: string, serviceUID: string, serviceName: string,
    author: GitlabOptionsAuthor, namespace: GitlabOptionsNamespace, remoteName: string) {
    this.apiBase = apiBase
    this.apiDomain = apiDomain
    this.token = token
    this.serviceUID = serviceUID
    this.author = author
    this.namespace = namespace
    this.remoteName = remoteName
  }
}

interface GitlabOptionsAuthor {
  username: string
  pass: string
  name: string
  email: string
}

interface GitlabOptionsNamespace {
  ID: string
  name: string
  description: string
}

class GitlabHandler {
  options: GitlabOptions
  FILE_PATH_CLONES: string
  constructor (options: GitlabOptions, filePathClones: string) {
    this.options = options
    this.setFilePaths(filePathClones)
  }

  // setFilePaths sets the path of
  setFilePaths (clones: string) {
    this.FILE_PATH_CLONES = path.resolve(clones)
    fse.mkdirp(this.FILE_PATH_CLONES)
  }

  getRepo (name: string): Promise<any> {
    return axios({
      method: 'get',
      url: `${this.options.apiBase}/projects/${this.options.namespace.name}%2f${name}`,
      headers: {
        'PRIVATE-TOKEN': this.options.token
      }
    })
    .then(response => {
      return response.data
    })
    .catch(err => {
      return Promise.reject(`Error retrieving ${this.options.serviceName} repository: ${err}`)
    })
  }

  // create generates a git repository on gitlab
  create (name: string, version: string, filePath: string): Promise<GitRepoDetails> {
    let repository: git.Repository
    let remote: git.Remote
    let branch: string = `${branchPrefix}${version}`
    let repoResponse: GitRepoDetails = new GitRepoDetails()

    // git init
    return git.Repository.init(filePath, 0)
    .then(repo => {
      repository = repo
      return
    })
    // git add .
    .then(() => {
      return repository.refreshIndex()
      .then(index => {
        return index.addAll()
        .then(() => {
          return index.write()
        })
        .then(() => {
          return index.writeTree()
        })
      })
      .catch(err => {
        return Promise.reject(`Error adding changes in ${this.options.serviceName} repo: ${err}`)
      })
    })
    // git commit
    .then(oid => {
      const date: number = Math.round(Date.now() / 1000)
      const author = git.Signature.create(this.options.author.name, this.options.author.email, date, 0)

      return repository.createCommit('HEAD', author, author, `Initial commit - ${this.options.namespace.description}`, oid, [])
        .catch(err => {
          return Promise.reject(`Error commiting changes in ${this.options.serviceName} repo: ${err}`)
        })
    })
    // git checkout -b branch_name
    .then(() => {
      return repository.getHeadCommit()
      .then(commit => {
        return git.Branch.create(repository, branch, commit, 0)
      })
      .catch(err => {
        return Promise.reject(`Error creating branch for ${this.options.serviceName} repo: ${err}`)
      })
    })
    // git remote add
    .then(() => {
      let repoURL: string = `https://${this.options.apiDomain}/${this.options.namespace.name}/${name}.git`
      return git.Remote.create(repository, this.options.remoteName, repoURL)
      .then(remoteResult => {
        remote = remoteResult
      })
      .catch(err => {
        return Promise.reject(`Error adding remote to ${this.options.serviceName} repo: ${err}`)
      })
    })
    // Create Gitlab repo
    .then(() => {
      return axios({
        method: 'post',
        url: `${this.options.apiBase}/projects`,
        params: {
          name: name,
          namespace_id: this.options.namespace.ID
        },
        headers: {
          'PRIVATE-TOKEN': this.options.token
        }
      })
      .then(response => {
        let data = response.data
        repoResponse.repoID = data.id
      })
      .catch(err => {
        return Promise.reject(`Error with ${this.options.serviceName} repo create request: ${err}`)
      })
    })
    // git push
    .then(() => {
      return remote.push([`refs/heads/${branch}:refs/heads/${branch}`], {
        callbacks: {
          credentials: () => { return git.Cred.userpassPlaintextNew(this.options.author.username, this.options.author.pass) }
        }
      })
      .catch(err => {
        return Promise.reject(`Error pushing to ${this.options.serviceName} repo: ${err}`)
      })
    })
    // Respond with repo details
    .then(() => {
      repoResponse.service = this.options.serviceUID
      repoResponse.name = name
      repoResponse.branch = branch
      repoResponse.repoURL = `https://${this.options.apiDomain}/${this.options.namespace.name}/${name}`
      repoResponse.tarball = `${repoResponse.repoURL}/repository/archive.tar.gz?ref=${branch}&private_token=${this.options.token}`
      return repoResponse
    })
    .catch(err => {
      // Delete repository
      this.delete(repoResponse.repoID)
      .catch(() => { /* Do nothing for now */ })
      return Promise.reject(`Error creating ${this.options.serviceName} repo: ${err}`)
    })
  }

  // update clones a repository, checks out a new branch, adds changes and pushes the branch to remote
  update (name: string, version: string, filePath: string): Promise<any> {
    let repository: git.Repository
    let remote: git.Remote
    let oid: git.Oid
    let cloneFilePath: string = `${this.FILE_PATH_CLONES}/${name}`
    let branch: string = `${branchPrefix}${version}`
    let repoResponse: GitRepoDetails = new GitRepoDetails()

    // Clone repository
    return git.Clone.clone(`https://${this.options.author.username}@${this.options.apiDomain}/${this.options.namespace.name}/${name}.git`, cloneFilePath, {
        fetchOpts: {
          callbacks: {
            credentials: () => { return git.Cred.userpassPlaintextNew(this.options.author.username, this.options.author.pass) }
          }
        }})
    // Copy .git file
    .then(() => {
      return fse.copy(`${cloneFilePath}/.git`, `${filePath}/.git`)
      .catch(err => {
        return Promise.reject(`Error copying .git from clone to file path: ${err}`)
      })
    })
    // Open git repo
    .then(() => {
      return git.Repository.open(`${filePath}/.git`)
      .then(repo => {
        repository = repo
        return
      })
    })
    // git add .
    .then(() => {
      return repository.refreshIndex()
      .then(index => {
        return index.addAll()
        .then(() => {
          return index.write()
        })
        .then(() => {
          return index.writeTree()
        })
      })
      .catch(err => {
        return Promise.reject(`Error adding changes to ${this.options.serviceName} repo: ${err}`)
      })
    })
    // git commit
    .then(oidResult => {
      oid = oidResult
      return git.Reference.nameToId(repository, 'HEAD')
      .then(head => {
        return repository.getCommit(head)
      })
    })
    .then(parent => {
      const date: number = Math.round(Date.now() / 1000)
      const author = git.Signature.create(this.options.author.name, this.options.author.email, date, 0)

      return repository.createCommit('HEAD', author, author, `Update app - ${this.options.namespace.description}`, oid, [parent])
        .catch(err => {
          return Promise.reject(`Error commiting changes to ${this.options.serviceName} repo: ${err}`)
        })
    })
    // git checkout -b branch_name
    .then(oidResult => {
      return repository.getCommit(oidResult)
      .then(commit => {
        return git.Branch.create(repository, branch, commit, 0)
      })
      .catch(err => {
        return Promise.reject(`Error creating branch for ${this.options.serviceName} repo: ${err}`)
      })
    })
    // git remote add
    .then(() => {
      let repoURL: string = `https://${this.options.apiDomain}/${this.options.namespace.name}/${name}.git`
      return git.Remote.create(repository, this.options.remoteName, repoURL)
      .then(remoteResult => {
        remote = remoteResult
      })
      .catch(err => {
        return Promise.reject(`Error adding remote to ${this.options.serviceName} repo: ${err}`)
      })
    })
    // git push
    .then(() => {
      return git.Remote.lookup(repository, this.options.remoteName, null)
      .then(remoteResult => {
        return remoteResult.push([`+refs/heads/${branch}:refs/heads/${branch}`], {
          callbacks: {
            credentials: () => { return git.Cred.userpassPlaintextNew(this.options.author.username, this.options.author.pass) }
          }
        })
      })
      .catch(err => {
        return Promise.reject(`Error pushing to ${this.options.serviceName} repo: ${err}`)
      })
    })
    // Retrieve repo id
    .then(() => {
      return this.getRepo(name)
      .then(response => {
        repoResponse.repoID = response.id
      })
    })
    // Respond with repo details
    .then(() => {
      cleanupClone(cloneFilePath)
      repoResponse.service = this.options.serviceUID
      repoResponse.name = name
      repoResponse.branch = branch
      repoResponse.repoURL = `https://${this.options.apiDomain}/${this.options.namespace.name}/${name}`
      repoResponse.tarball = `${repoResponse.repoURL}/repository/archive.tar.gz?ref=${branch}&private_token=${this.options.token}`
      return repoResponse
    })
    .catch(err => {
      cleanupClone(cloneFilePath)
      return Promise.reject(`Error updating ${this.options.serviceName} repo: ${err}`)
    })
  }

  // delete removes a repository from gitlab
  delete (repoID: string): Promise<boolean> {
    // https://docs.gitlab.com/ce/api/projects.html#remove-project
    return axios({
      method: 'delete',
      url: `${this.options.apiBase}/projects/${repoID}`,
      headers: {
        'PRIVATE-TOKEN': this.options.token
      }
    })
    .then(response => {
      return Promise.resolve(true)
    })
    .catch(err => {
      return Promise.reject(`Error deleting ${this.options.serviceName} repo: ${err}`)
    })
  }
}

// cleanupClone removes cloned repositories
function cleanupClone (filePath: string) {
  fse.remove(filePath)
  .catch(() => { /* Do nothing for now */ })
}

export {
  GitlabHandler,
  GitlabOptions,
  GitlabOptionsAuthor,
  GitlabOptionsNamespace
}
