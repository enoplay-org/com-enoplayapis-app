import axios from 'axios'
import * as https from 'https'
import * as http from 'http'
import * as url from 'url'

import {
  HostDetails,
  HostDetailsStatus,
  APP_STATUS_PENDING,
  APP_STATUS_ACTIVE,
  APP_STATUS_ERROR
} from '../domain/app'

const herokuApiBase: string = 'https://api.heroku.com'

// Wait 500 milliseconds for any status request
const statusTimeout: number = 500

class HerokuOptions {
  token: string
  serviceUID: string
  constructor (token: string, serviceUID: string) {
    this.token = token
    this.serviceUID = serviceUID
  }
}

class HerokuHandler {
  options: HerokuOptions
  constructor (options: HerokuOptions) {
    this.options = options
  }

  // getSetupStatus retrieves an app's status during setup - before it is operating
  getSetupStatus (appSetupID: string): Promise<HostDetailsStatus> {
    // https://devcenter.heroku.com/articles/platform-api-reference#app-setup-info

    let statusResponse: HostDetailsStatus = new HostDetailsStatus()

    return axios({
      method: 'get',
      url: `${herokuApiBase}/app-setups/${appSetupID}`,
      headers: {
        Accept: 'application/vnd.heroku+json; version=3',
        Authorization: `Bearer ${this.options.token}`
      }
    })
    .then(response => {
      let data = response.data
      switch (data['status']) {
        case 'failed':
          statusResponse.label = APP_STATUS_ERROR
          break
        case 'succeeded':
          statusResponse.label = APP_STATUS_ACTIVE
          break
        default:
          statusResponse.label = APP_STATUS_PENDING
      }
      statusResponse.message = data['failure_message']
    })
    .then(() => {
      return Promise.resolve(statusResponse)
    })
    .catch(err => {
      return Promise.reject(`Failed to get Heroku app setup status: ${err}`)
    })
  }

  // getBuildStatus retrieves an app's status during a new build
  getBuildStatus (appID: string): Promise<HostDetailsStatus> {
    // https://devcenter.heroku.com/articles/platform-api-reference#build-result

    let statusResponse: HostDetailsStatus = new HostDetailsStatus()

    return axios({
      method: 'get',
      url: `${herokuApiBase}/apps/${appID}/builds`,
      headers: {
        Accept: 'application/vnd.heroku+json; version=3',
        Authorization: `Bearer ${this.options.token}`
      }
    })
    .then(response => {
      let data = response.data
      let latestBuild = data[data.length - 1]

      switch (latestBuild['status']) {
        case 'failed':
          statusResponse.label = APP_STATUS_ERROR
          break
        case 'succeeded':
          statusResponse.label = APP_STATUS_ACTIVE
          break
        default:
          statusResponse.label = APP_STATUS_PENDING
      }
      statusResponse.message = latestBuild['failure_message']

      return new Promise<string>((resolve, reject) => {
        let body: string = ''
        let responseEnded: boolean = false

        let streamURL = url.parse(`${latestBuild['output_stream_url']}`)

        let requestOptions: https.RequestOptions = {
          hostname: streamURL.hostname,
          port: Number(streamURL.port),
          path: streamURL.path
        }

        https.get(requestOptions, (res: http.IncomingMessage) => {
          res.on('data', (chunk: string|Buffer) => {
            body += chunk
          })

          // On stream end, return response
          res.on('end', () => {
            responseEnded = true
            res.removeAllListeners()
            resolve(body)
          })

          // Stop streaming after a few milliseconds and return response
          setTimeout(() => {
            if (responseEnded) {
              return
            }
            res.removeAllListeners()
            resolve(body)
          }, statusTimeout)
        })
      })
      .then(logResponse => {
        statusResponse.buildLogDetails = logResponse
      })
      .catch(err => {
        return Promise.reject(`Error retrieving build output stream from url ${latestBuild['output_stream_url']}: ${err}`)
      })
    })
    .then(() => {
      return Promise.resolve(statusResponse)
    })
    .catch(err => {
      return Promise.reject(`Error retrieving Heroku app build status: ${err}`)
    })
  }

  // getRuntimeStatus retrieves an app's status while it is operating
  getRuntimeStatus (appID: string): Promise<string> {
    // https://devcenter.heroku.com/articles/platform-api-reference#log-session-create

    return axios({
      method: 'post',
      url: `${herokuApiBase}/apps/${appID}/log-sessions`,
      headers: {
        Accept: 'application/vnd.heroku+json; version=3',
        Authorization: `Bearer ${this.options.token}`
      },
      data: {
        lines: 50
      }
    })
    .then(response => {
      let data = response.data

      return axios({
        method: 'get',
        url: `${data['logplex_url']}`
      })
      .then(logResponse => {
        return logResponse.data
      })
    })
    .then(logResponse => {
      return Promise.resolve(logResponse)
    })
    .catch(err => {
      return Promise.reject(`Error retrieving Heroku app runtime status: ${err}`)
    })
  }

  // getApp retrieves the details of an app
  getApp (appID: string): Promise<any> {
    return axios({
      method: 'get',
      url: `${herokuApiBase}/apps/${appID}`,
      headers: {
        Accept: 'application/vnd.heroku+json; version=3',
        Authorization: `Bearer ${this.options.token}`
      }
    })
    .then(response => {
      return response.data
    })
    .catch(err => {
      return Promise.reject(`Failed to get Heroku app: ${err}`)
    })
  }

  // create generates a new app from a given git repository
  create (repoURL: string): Promise<HostDetails> {
    let hostResponse: HostDetails = new HostDetails()
    return axios({
      method: 'post',
      url: `${herokuApiBase}/app-setups`,
      data: {
        source_blob: {
          url: repoURL
        }
      },
      headers: {
        Accept: 'application/vnd.heroku+json; version=3',
        Authorization: `Bearer ${this.options.token}`
      }
    })
    .then(response => {
      let data = response.data
      hostResponse.appSetupID = data.id
      hostResponse.service = this.options.serviceUID
      hostResponse.appID = data.app.id
      hostResponse.webURL = `https://${data.app.name}.herokuapp.com`
      return hostResponse
    })
    .catch(err => {
      // Delete host application
      this.delete(hostResponse.appID)
      return Promise.reject(`Error creating Heroku app: ${err}`)
    })
  }

  // update deploys a git repository over an existing app
  update (appID: string, repoURL: string): Promise<HostDetails> {
    // https://devcenter.heroku.com/articles/platform-api-reference#app-update

    let hostResponse: HostDetails = new HostDetails()
    return axios({
      method: 'post',
      url: `${herokuApiBase}/apps/${appID}/builds`,
      data: {
        source_blob: {
          url: repoURL
        }
      },
      headers: {
        Accept: 'application/vnd.heroku+json; version=3',
        Authorization: `Bearer ${this.options.token}`
      }
    })
    .then(response => {
      let data = response.data
      hostResponse.service = this.options.serviceUID
      hostResponse.appID = data.app.id
    })
    // Retrieve app details
    .then(() => {
      return this.getApp(appID)
    })
    // Respond
    .then(appResponse => {
      hostResponse.webURL = appResponse['web_url']
      return hostResponse
    })
    .catch(err => {
      return Promise.reject(`Error updating Heroku app: ${err}`)
    })
  }

  // delete destroys an app
  delete (appID: string): Promise<boolean> {
    // https://devcenter.heroku.com/articles/platform-api-reference#app-delete
    return axios({
      method: 'delete',
      url: `${herokuApiBase}/apps/${appID}`,
      headers: {
        Accept: 'application/vnd.heroku+json; version=3',
        Authorization: `Bearer ${this.options.token}`
      }
    })
    .then(response => {
      return Promise.resolve(true)
    })
    .catch(err => {
      return Promise.reject(`Error deleting Heroku app: ${err}`)
    })
  }
}

export {
  HerokuOptions,
  HerokuHandler
}
