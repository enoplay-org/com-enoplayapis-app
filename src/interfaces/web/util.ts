import * as express from 'express'
import * as extract from 'extract-zip'
import * as fse from 'fs-extra'
import * as path from 'path'

const ZIP_FILE_FIELDNAME = 'zipFile'

class Utility {
  FILE_PATH_UPLOADS: string
  FILE_PATH_UNZIPPED: string

  setFilePaths (uploads: string, unzipped: string) {
    this.FILE_PATH_UPLOADS = path.resolve(uploads)
    this.FILE_PATH_UNZIPPED = path.resolve(unzipped)

    fse.mkdirp(this.FILE_PATH_UPLOADS)
    .catch(err => {
      console.error(`Error creating file path upload directory: ${err}`)
    })
    fse.mkdirp(this.FILE_PATH_UNZIPPED)
    .catch(err => {
      console.error(`Error creating file path unzipped directory: ${err}`)
    })
  }

  render (req: express.Request, res: express.Response, status: number, data: any) {
    res.status(status)
    res.json(data)
  }

  renderError (req: express.Request, res: express.Response, status: number, message: string) {
    res.status(status)
    res.json({
      message,
      success: false
    })
  }

  unzipFile (fileName: string): Promise<any> {
    return new Promise((resolve, reject) => {
      extract(`${this.FILE_PATH_UPLOADS}/${fileName}.zip`, {
        dir: `${this.FILE_PATH_UNZIPPED}/${fileName}`
      }, err => {
        if (err) {
          reject(`Error unzipping file: ${err}`)
        } else {
          resolve()
        }
      })
    })
  }
}

export {
  Utility,
  ZIP_FILE_FIELDNAME
}
