import { Utility } from './util'

import { MetaHandler } from './handleMeta'
import { AppHandler } from './handleApp'
import { Middleware } from './middleware'

import { AppInteractor, SessionTokenInteractor, GameInteractor, UserInteractor } from './interactors'

class WebHandlerOptions {
  filePathUpload: string
  filePathUnzip: string
  constructor (filePathUpload: string, filePathUnzip: string) {
    this.filePathUpload = filePathUpload
    this.filePathUnzip = filePathUnzip
  }
}

class WebHandler {
  options: WebHandlerOptions
  meta: MetaHandler
  app: AppHandler
  mw: Middleware
  constructor (options: WebHandlerOptions) {
    this.options = options
    this.meta = new MetaHandler()
    this.app = new AppHandler()
    this.mw = new Middleware()

    let util = new Utility()
    util.setFilePaths(options.filePathUpload, options.filePathUnzip)
    this.meta.util = util
    this.app.util = util
    this.mw.util = util
  }

  setAppInteractor (appInteractor: AppInteractor) {
    this.app.appInteractor = appInteractor
  }

  setSessionTokenInteractor (sessionTokenInteractor: SessionTokenInteractor) {
    this.mw.sessionTokenInteractor = sessionTokenInteractor
  }

  setUserInteractor (userInteractor: UserInteractor) {
    this.app.userInteractor = userInteractor
  }

  setGameInteractor (gameInteractor: GameInteractor) {
    this.app.gameInteractor = gameInteractor
  }
}

export {
  WebHandlerOptions,
  WebHandler
}
