import { Request, Response } from './express.d'

import { Utility } from './util'

interface GetIndexResponseV0 {
  message: string
  versions: string
  success: boolean
}

const API_VERSIONS = ['v0']

class MetaHandler {
  util: Utility
  getIndex (req: Request, res: Response) {
    let response: GetIndexResponseV0 = {
      message: `Welcome to app.enoplayapis.com`,
      versions: `Supported api versions: ${API_VERSIONS.toString()}`,
      success: true
    }
    this.util.render(req, res, 200, response)
  }

  getCertBotKey (req: Request, res: Response) {
    res.write('place_certbot_key_here')
  }
}

export {
  MetaHandler
}
