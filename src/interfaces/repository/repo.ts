import { GitRepoDetails, HostDetails, HostDetailsStatus } from '../../domain/app'

type Query = { [key: string]: any }
type Change = {
  $set?: Query
  $unset?: Query
}

interface DbHandler {
  insert (name: string, obj: any): Promise<any>
  update (name: string, query: Query, change: Change): Promise<any>
  findOne (name: string, query: Query): Promise<any>
  removeOne (name: string, query: Query): Promise<boolean>
  exists (name: string, query: Query): Promise<boolean>
}

class DbRepo {
  dbHandlers: { [key: string]: DbHandler }
  dbHandler: DbHandler
  constructor (dbHandlers: { [key: string]: DbHandler }, dbHandler: DbHandler) {
    this.dbHandlers = dbHandlers
    this.dbHandler = dbHandler
  }
}

interface GitRepoHandler {
  create (name: string, version: string, filePath: string): Promise<GitRepoDetails>
  update (name: string, version: string, filePath: string): Promise<GitRepoDetails>
  delete (repoID: string): Promise<boolean>
}

interface HostHandler {
  getSetupStatus (appSetupID: string): Promise<HostDetailsStatus>
  getBuildStatus (appID: string): Promise<HostDetailsStatus>
  getRuntimeStatus (appID: string): Promise<string>
  create (repoURL: string): Promise<HostDetails>
  update (appID: string, repoURL: string): Promise<HostDetails>
  delete (appID: string): Promise<boolean>
}

interface MediaHandler {
  delete (sessionToken: string, uid: string): Promise<boolean>
}

export {
  DbHandler,
  DbRepo,
  Query,
  Change,
  GitRepoHandler,
  HostHandler,
  MediaHandler
}
