import { DbRepo, DbHandler, MediaHandler, Query, Change } from './repo'
import { Game } from '../../domain/game'

const GAME_COLLECTION = 'games'
const GAME_DELETED_COLLECTION = 'games_deleted'
const GAME_USER_PURCHASE_COLLECTION = 'games_user_purchase'
const GAME_USER_BAN_COLLECTION = 'games_user_ban'

class DbGameRepo extends DbRepo {
  mediaHandler: MediaHandler
  constructor (dbHandlers: { [key: string]: DbHandler }, mediaHandler: MediaHandler) {
    super(dbHandlers, dbHandlers[DbGameRepo.getClassName()])
    this.mediaHandler = mediaHandler
  }

  static getClassName (): string { return 'DbGameRepo' }

  getByUID (uid: string): Promise<Game> {
    return new Promise((resolve, reject) => {
      return this.dbHandler.findOne(GAME_COLLECTION, { uid: uid })
      .then(response => {
        let game: Game = response
        if (!game) {
          return Promise.reject('Error finding game from repo')
        }
        return Promise.resolve(game)
      })
      .then(gameResponse => {
        resolve(gameResponse)
      })
      .catch(err => {
        reject(`Error retrieving game by uid from repo: ${err}`)
      })
    })
  }

  getByGID (gid: string): Promise<Game> {
    return new Promise((resolve, reject) => {
      return this.dbHandler.findOne(GAME_COLLECTION, { gid: gid })
      .then(response => {
        let game: Game = response
        if (!game) {
          return Promise.reject('Error finding game from repo')
        }
        return Promise.resolve(game)
      })
      .then(gameResponse => {
        resolve(gameResponse)
      })
      .catch(err => {
        reject(`Error retrieving game by gid from repo: ${err}`)
      })
    })
  }

  updateApp (game: Game): Promise<Game> {
    return new Promise((resolve, reject) => {
      let query: Query = { app: game.app }
      let change: Change = {
        $set: query
      }
      return this.dbHandler.update(GAME_COLLECTION, { uid: game.uid }, change)
      .then(response => {
        let game: Game = response
        if (!game) {
          return Promise.reject('Error finding game from repo')
        }
        return Promise.resolve(game)
      })
      .then(gameResponse => {
        resolve(gameResponse)
      })
      .catch(err => {
        reject(`Error updating game app in repo: ${err}`)
      })
    })
  }

  delete (sessionToken: string, game: Game): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // Add Game to deleted collection
      return this.dbHandler.insert(GAME_DELETED_COLLECTION, game)
      // Delete Game media
      .then(() => {
        return this.deleteThumbnail(sessionToken, game)
      })
      .then(() => {
        return this.deleteImages(sessionToken, game)
      })
      // Continue regardless of error
      .catch(() => {
        return Promise.resolve()
      })
      // Remove Game history
      .then(() => {
        this.dbHandler.removeOne(GAME_USER_PURCHASE_COLLECTION, { 'gameUID': game.uid })
        .catch(() => { /* Do nothing for now */ })
        this.dbHandler.removeOne(GAME_USER_BAN_COLLECTION, { 'gameUID': game.uid })
        .catch(() => { /* Do nothing for now */ })
      })
      // Remove Game from collection
      .then(() => {
        return this.dbHandler.removeOne(GAME_COLLECTION, { uid: game.uid })
      })
      .then(isDeleted => {
        resolve(isDeleted)
      })
      .catch(err => {
        reject(`Error deleting game: ${err}`)
      })
    })
  }

  deleteThumbnail (sessionToken: string, game: Game): Promise<Game> {
    return new Promise((resolve, reject) => {
      // Delete thumbnail
      this.mediaHandler.delete(sessionToken, game.media.thumbnail.uid)
      .catch(() => { /* Do nothing for now */ })

      game.media.thumbnail.uid = ''
      game.media.thumbnail.source = ''

      let query: Query = { 'media.thumbnail': game.media.thumbnail }
      let change: Change = {
        $set: query
      }

      // Update Game
      return this.dbHandler.update(GAME_COLLECTION, { uid: game.uid }, change)
      .then(response => {
        let game: Game = response
        if (!game) {
          return Promise.reject('Error finding game from repo')
        }
        return Promise.resolve(game)
      })
      .then(gameResponse => {
        resolve(gameResponse)
      })
      .catch(err => {
        reject(`Error updating game thumbnail in repo: ${err}`)
      })
    })
  }

  deleteImages (sessionToken: string, game: Game): Promise<Game> {
    return new Promise((resolve, reject) => {
      // Delete media images
      for (let imageUID in game.media.images) {
        if (!game.media.images.hasOwnProperty(imageUID)) {
          continue
        }
        this.mediaHandler.delete(sessionToken, imageUID)
        .catch(() => { /* Do nothing for now */ })
      }

      game.media.images = {}

      let query: Query = { 'media.images': game.media.images }
      let change: Change = {
        $set: query
      }

      // Update Game
      return this.dbHandler.update(GAME_COLLECTION, { uid: game.uid }, change)
      .then(response => {
        let game: Game = response
        if (!game) {
          return Promise.reject('Error finding game from repo')
        }
        return Promise.resolve(game)
      })
      .then(gameResponse => {
        resolve(gameResponse)
      })
      .catch(err => {
        reject(`Error updating game images in repo: ${err}`)
      })
    })
  }
}

export {
  DbGameRepo
}
