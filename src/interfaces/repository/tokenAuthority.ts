
class TokenAuthorityOptions {
  privateSigningKey: string
  publicSigningKey: string
  constructor (privateKey: string, publicKey: string) {
    this.privateSigningKey = privateKey
    this.publicSigningKey = publicKey
  }
}

export {
  TokenAuthorityOptions
}
