import * as path from 'path'
import * as fse from 'fs-extra'

import {
  App,
  AppStatus,
  AppVersionHistory,
  AppVersionHistoryItem,
  AppRepository,
  APP_LANGUAGE_SPA,
  APP_LANGUAGE_NODE,
  APP_LANGUAGE_GO
} from '../domain/app'

import {
  User,
  UserRepository,
  UserAppPublishHistoryItem
} from '../domain/user'

import {
  Game,
  GameRepository,
  GameApp
} from '../domain/game'

class AppInteractorOptions {
  wwwHostURL: string
  mobileHostURL: string
  apiHostURL: string
  staticHostURL: string
  cdnHostURL: string
  constructor (wwwHostURL: string, mobileHostURL: string, apiHostURL: string, staticHostURL: string, cdnHostURL: string) {
    this.wwwHostURL = wwwHostURL
    this.mobileHostURL = mobileHostURL
    this.apiHostURL = apiHostURL
    this.staticHostURL = staticHostURL
    this.cdnHostURL = cdnHostURL
  }
}

class AppInteractor {
  options: AppInteractorOptions
  appRepository: AppRepository
  userRepository: UserRepository
  gameRepository: GameRepository
  constructor (options: AppInteractorOptions) {
    this.options = options
    initSPAServer()
  }
  setAppRepository (appRepository: AppRepository) {
    this.appRepository = appRepository
  }

  setUserRepository (userRepository: UserRepository) {
    this.userRepository = userRepository
  }

  setGameRepository (gameRepository: GameRepository) {
    this.gameRepository = gameRepository
  }

  getByUID (uid: string): Promise<App> {
    return this.appRepository.getByUID(uid)
    .catch(err => {
      return Promise.reject(`Error retrieving app by uid from manage: ${err}`)
    })
  }

  getByAID (aid: string): Promise<App> {
    return this.appRepository.getByAID(aid)
    .catch(err => {
      return Promise.reject(`Error retrieving app by aid from manage: ${err}`)
    })
  }

  getStatus (app: App): Promise<AppStatus> {
    return this.appRepository.getStatus(app)
    .catch(err => {
      return Promise.reject(`Error retrieving app status from manage: ${err}`)
    })
  }

  getVersions (app: App): Promise<AppVersionHistory> {
    return this.appRepository.getVersionHistory(app.uid)
    .catch(err => {
      return Promise.reject(`Error retrieving app versions from manage: ${err}`)
    })
  }

  create (game: Game, publisher: User, language: string, filePath: string): Promise<App> {
    return new Promise((resolve, reject) => {
      // Validate input
      language = mapLanguage(language)
      if (!isValidLanguage(language)) {
        return Promise.reject(`Invalid App language: ${language}`)
      }

      // Initialize App
      let app: App = new App()
      app.title = game.title
      app.publisher = { uid: publisher.uid, username: publisher.username }
      app.game = { uid: game.uid, gid: game.gid }

      let appDetails: AppDetails = {
        title: game.title,
        language: language,
        game,
        publisher,
        options: this.options
      }

      // Reserve aid
      return this.reserveAID()
      .then(aid => {
        app.aid = aid
        return aid
      })
      // Update Game w/ aid to prevent further create attempts
      .then(appAID => {
        if (!game.app) {
          game.app = new GameApp()
        }
        game.app.aid = appAID
        return this.gameRepository.updateApp(game)
      })
      // Update index.html
      .then(() => {
        return ensureIndexHTML(filePath)
        .then(() => {
          return updateIndexHTML(filePath, appDetails)
        })
      })
      // Add SPA Server code
      .then(() => {
        return addSPAServer(filePath, appDetails)
      })
      // Create App
      .then(() => {
        return this.appRepository.create(app, language, filePath)
        .then(appResponse => {
          app = appResponse
        })
      })
      // Update User's App publish history
      .then(() => {
        let appPublishItem: UserAppPublishHistoryItem = {
          appUID: app.uid,
          dateAdded: new Date()
        }
        return this.userRepository.publishApp(app.publisher.uid, appPublishItem)
      })
      // Update Game App
      .then(() => {
        return this.appRepository.getVersion(app.uid, app.versionsActive.prod)
        .then(appVersionResponse => {
          game.app.uid = app.uid
          game.app.aid = app.aid
          game.app.webURL = appVersionResponse.hostDetails.webURL
          return this.gameRepository.updateApp(game)
        })
      })
      // Return App
      .then(() => {
        resolve(app)
      })
      .catch(err => {
        // Delete App
        this.deleteWithForce(app)
        .catch(() => { /* Do nothing for now */ })
        reject(`Error creating app from manage: ${err}`)
      })
    })
  }

  updateVersionProd (app: App, versionUID: string): Promise<App> {
    return new Promise((resolve, reject) => {

      // Ensure version isn't already prod
      let prodVersionUID = app.versionsActive.prod

      if (versionUID === prodVersionUID) {
        reject('App is already in production (live)')
        return
      }

      // Update versionsActive
      if (versionUID === app.versionsActive.dev) {
        // Swap dev and prod versions
        app.versionsActive.prod = app.versionsActive.dev
        app.versionsActive.dev = prodVersionUID
      } else {
        app.versionsActive.dev = versionUID
      }

      let appVersion: AppVersionHistoryItem

      // Validate versionUID
      return this.appRepository.getVersion(app.uid, versionUID)
      .then(appVersionResponse => {
        appVersion = appVersionResponse
      })
      // Update game app webURL
      .then(() => {
        return this.gameRepository.getByUID(app.game.uid)
      })
      .then(gameResponse => {
        gameResponse.app.webURL = appVersion.hostDetails.webURL
        return this.gameRepository.updateApp(gameResponse)
      })
      // Update App
      .then(() => {
        return this.appRepository.updateVersionsActive(app)
      })
      // Respond
      .then(appResponse => {
        resolve(appResponse)
      })
      .catch(err => {
        reject(`Error updating app version prod from manage: ${err}`)
      })
    })
  }

  updateFile (app: App, publisher: User, language: string, filePath: string): Promise<App> {
    return new Promise((resolve, reject) => {
      // Ensure language is valid
      language = mapLanguage(language)
      if (!isValidLanguage(language)) {
        return reject(`Invalid App language: ${language}`)
      }

      // Setup host appIDs
      let prodHostAppID: string
      let devHostAppID: string
      let doesDevHostAppExist: boolean = false

      let details: AppDetails
      let appVersionHistory: AppVersionHistory

      return this.appRepository.getVersionHistory(app.uid)
      .then(historyResponse => {
        appVersionHistory = historyResponse

        if (appVersionHistory.versions[app.versionsActive.prod]) {
          prodHostAppID = appVersionHistory.versions[app.versionsActive.prod].hostDetails.appID
        }
        if (appVersionHistory.versions[app.versionsActive.dev]) {
          devHostAppID = appVersionHistory.versions[app.versionsActive.dev].hostDetails.appID
        }

        // Check if prod and dev host apps are unique
        doesDevHostAppExist = devHostAppID && (prodHostAppID !== devHostAppID)
      })
      // Retrieve App Game
      .then(() => {
        return this.gameRepository.getByUID(app.game.uid)
      })
      .then(game => {
        if (!game) {
          return Promise.reject('Error finding app game')
        }
        details = {
          title: game.title,
          language: language,
          game,
          publisher,
          options: this.options
        }
      })
      // Add SPA server code
      .then(() => {
        return addSPAServer(filePath, details)
      })
      // Update App
      .then(() => {
        // Update existing dev host app
        if (doesDevHostAppExist) {
          return this.appRepository.updateFile(app, language, filePath, devHostAppID)
        // Create new dev host app
        } else {
          return this.appRepository.updateFile(app, language, filePath)
        }
      })
      // Return App
      .then(appResponse => {
        resolve(appResponse)
      })
      .catch(err => {
        reject(`Error updating app file from manage: ${err}`)
      })
    })
  }

  deleteWithForce (app: App): Promise<boolean> {
    return new Promise((resolve, reject) => {
      return this.appRepository.deleteWithForce(app)
      // Remove App from User's publish history
      .then(() => {
        return this.userRepository.unpublishApp(app.publisher.uid, app.uid)
      })
      .then(() => {
        resolve(true)
      })
      .catch(err => {
        reject(`Error deleting app from manage: ${err}`)
      })
    })
  }

  reserveAID (): Promise<string> {
    let aid: string = generateAID()
    return new Promise((resolve, reject) => {
      return this.appRepository.existsByAID(aid)
      .then(exists => {
        if (exists) {
          resolve(this.reserveAID())
        } else {
          resolve(aid)
        }
      })
      .catch(err => {
        reject(`Error generating app aid: ${err}`)
      })
    })
  }
}

const appJSON = 'app.json'
const authIndexHTML = 'auth-index.html'
const epMiddlewareJS = 'ep-middleware.js'
const epServerJS = 'ep-server.js'
const packageJSON = 'package.json'

let spaServer: { [key: string]: string } = {
  [appJSON]: '',
  [authIndexHTML]: '',
  [epMiddlewareJS]: '',
  [epServerJS]: '',
  [packageJSON]: ''
}

const appFiles = path.resolve(__dirname, './app_files')

function initSPAServer () {
  fse.readFile(path.resolve(__dirname, `${appFiles}/spa_server/${appJSON}`))
  .then(buffer => {
    spaServer[appJSON] = buffer.toString()
  })

  fse.readFile(path.resolve(__dirname, `${appFiles}/spa_server/${authIndexHTML}`))
  .then(buffer => {
    spaServer[authIndexHTML] = buffer.toString()
  })

  fse.readFile(path.resolve(__dirname, `${appFiles}/spa_server/${epMiddlewareJS}`))
  .then(buffer => {
    spaServer[epMiddlewareJS] = buffer.toString()
  })

  fse.readFile(path.resolve(__dirname, `${appFiles}/spa_server/${epServerJS}`))
  .then(buffer => {
    spaServer[epServerJS] = buffer.toString()
  })

  fse.readFile(path.resolve(__dirname, `${appFiles}/spa_server/${packageJSON}`))
  .then(buffer => {
    spaServer[packageJSON] = buffer.toString()
  })
}

interface AppDetails {
  title: string
  language: string
  game: Game
  publisher: User
  options: AppInteractorOptions
}

// addSPAServer adds a server to a spa App
function addSPAServer (filePath: string, details: AppDetails) {
  let server = JSON.parse(JSON.stringify(spaServer))
  server[appJSON] = server[appJSON].replace(/{{ title }}/g, details.title)
  server[appJSON] = server[appJSON].replace(/{{ language }}/g, `${details.language}`)
  server[authIndexHTML] = server[authIndexHTML].replace(/{{ gameUID }}/g, details.game.uid)
  server[authIndexHTML] = server[authIndexHTML].replace(/{{ title }}/g, details.title)
  server[authIndexHTML] = server[authIndexHTML].replace(/{{ thumbnail }}/g, `${details.options.staticHostURL}/games/${details.game.gid}/thumbnail`)
  server[authIndexHTML] = server[authIndexHTML].replace(/{{ publisherAlias }}/g, details.publisher.alias)
  server[authIndexHTML] = server[authIndexHTML].replace(/{{ publisherUsername }}/g, details.publisher.username)
  server[authIndexHTML] = server[authIndexHTML].replace(/{{ publisherIcon }}/g, `${details.options.staticHostURL}/users/${details.publisher.username}/icon`)
  server[authIndexHTML] = server[authIndexHTML].replace(/{{ wwwHostURL }}/g, details.options.wwwHostURL)
  server[authIndexHTML] = server[authIndexHTML].replace(/{{ mobileHostURL }}/g, details.options.mobileHostURL)
  server[authIndexHTML] = server[authIndexHTML].replace(/{{ apiHostURL }}/g, details.options.apiHostURL)
  server[authIndexHTML] = server[authIndexHTML].replace(/{{ staticHostURL }}/g, details.options.staticHostURL)
  server[authIndexHTML] = server[authIndexHTML].replace(/{{ cdnHostURL }}/g, details.options.cdnHostURL)
  server[epMiddlewareJS] = server[epMiddlewareJS].replace(/{{ gid }}/g, details.game.gid)
  server[epMiddlewareJS] = server[epMiddlewareJS].replace(/{{ wwwHostURL }}/g, details.options.wwwHostURL)
  server[epMiddlewareJS] = server[epMiddlewareJS].replace(/{{ mobileHostURL }}/g, details.options.mobileHostURL)
  server[epMiddlewareJS] = server[epMiddlewareJS].replace(/{{ apiHostURL }}/g, details.options.apiHostURL)
  server[packageJSON] = server[packageJSON].replace(/{{ publisherAlias }}/g, details.publisher.alias)
  server[packageJSON] = server[packageJSON].replace(/{{ publisherUsername }}/g, details.publisher.username)

  let promises = []
  for (let fileName in server) {
    if (server.hasOwnProperty(fileName)) {
      promises.push(
        fse.writeFile(`${filePath}/${fileName}`, server[fileName])
      )
    }
  }

  return Promise.all(promises)
}

function ensureIndexHTML (filePath: string) {
  return fse.pathExists(`${filePath}/index.html`)
  .then(exists => {
    if (!exists) {
      return Promise.reject('index.html doesn\'t exist')
    }
  })
}

// updateIndexHTML updates index.html with a script for periodically re-authenticating a user
function updateIndexHTML (filePath: string, details: AppDetails) {
  let indexHTML: string = ''

  // Read index.html
  return fse.readFile(`${filePath}/index.html`)
  .then(buffer => {
    indexHTML = buffer.toString()
    let script1: string = ''
    let script2: string = ''

    // Setup script 1 (auth environment object)
    const authEnvObjectName = 'enoplayAuthEnvV0'
    let authEnv = {
      gameUID: details.game.uid,
      isAuthIndex: false,
      wwwHostURL: details.options.wwwHostURL,
      mobileHostURL: details.options.mobileHostURL,
      apiHostURL: details.options.apiHostURL,
      staticHostURL: details.options.staticHostURL,
      cdnHostURL: details.options.cdnHostURL
    }

    script1 = `\t<script type="text/javascript">window.${authEnvObjectName}=${JSON.stringify(authEnv)}</script>`

    // Setup script 2 (js/client/game/auth.js)
    script2 = `<script type="text/javascript" src="${details.options.cdnHostURL}/js/client/game/auth.js"></script>\n\t`

    if (indexHTML.indexOf('</head>') >= 0) {
      indexHTML = injectText(indexHTML, script1, indexHTML.indexOf('</head>'))
      indexHTML = injectText(indexHTML, script2, indexHTML.indexOf('</head>'))
    } else if (indexHTML.indexOf('</body>') >= 0) {
      indexHTML = injectText(indexHTML, script1, indexHTML.indexOf('</body>'))
      indexHTML = injectText(indexHTML, script2, indexHTML.indexOf('</body>'))
    } else if (indexHTML.indexOf('</html>') >= 0) {
      indexHTML += `<html><head>${script1}${script2}</head></html>`
    } else {
      indexHTML += `<html><head>${script1}${script2}</head></html>`
    }
  })
  // Update index.html
  .then(() => {
    return fse.writeFile(`${filePath}/index.html`, indexHTML)
  })
}

const maxAIDLength = 16
const aidCharacters = '01234556789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-'

function generateAID (): string {
  let aid: string = ''

  for (let i = 0; i < maxAIDLength; i++) {
    let location: number = Math.floor(Math.random() * aidCharacters.length)
    aid += aidCharacters.charAt(location)
  }
  return aid
}

function isValidLanguage (lang: string): boolean {
  switch (lang) {
    case APP_LANGUAGE_NODE: return true
    /* Not available yet
    case APP_LANGUAGE_GO: return true
    */
    default: return false
  }
}

function mapLanguage (lang: string): string {
  switch (lang) {
    case APP_LANGUAGE_SPA: return APP_LANGUAGE_NODE
    case APP_LANGUAGE_NODE: return APP_LANGUAGE_NODE
    case APP_LANGUAGE_GO: return APP_LANGUAGE_GO
    default: return ''
  }
}

function injectText (main: string, text: string, index: number): string {
  if (index < 0) {
    return main
  }
  return `${main.slice(0, index)}${text}${main.slice(index)}`
}

export {
  AppInteractorOptions,
  AppInteractor,

  // exports for testing
  AppDetails,
  ensureIndexHTML,
  updateIndexHTML
}
