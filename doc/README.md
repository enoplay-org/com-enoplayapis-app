# ✏️ Docs

> Getting started with com-enoplayapis-app.

## 🕌 Architecture Overview

<img src="https://8thlight.com/blog/assets/posts/2012-08-13-the-clean-architecture/CleanArchitecture-5c6d7ec787d447a81b708b73abba1680.jpg">

## ✨ Inspiration

🛀 Robert Martin's [Clean Architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html)

📎 [Example](https://github.com/jaijiv/handybid) using the clean architecture with golang

📎 [Example](https://github.com/sogko/slumber) writing a web API server with golang
